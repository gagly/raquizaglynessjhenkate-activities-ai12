import React from "react";
import { StatusBar } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import homescreen from "./screen/homescreen";
import activity2 from "./screen/activity2";
import profilescreen from "./screen/profilescreen";


const App = () => {
  const Tab = createBottomTabNavigator();
  return (
    <>
      <StatusBar backgroundColor="#A1887F" barStyle="dark-content" />
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, size, colour }) => {
              let iconName;
              if (route.name === "Home") {
                iconName = focused ? "ios-home" : "ios-home-outline";
                size = focused ? size + 7 : size + 6;
              } else if (route.name === "My To do list") {
                iconName = focused ? "list-circle" : "list-circle-outline";
                size = focused ? size + 7 : size + 6;
              } else if (route.name === "Completed Tasks") {
                iconName = focused ? "list-sharp" : "list-outline";
                size = focused ? size + 7 : size + 6;
              }
              return <Ionic name={iconName} size={size} colour={colour} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: "black",
            inactiveTintColor: "black",
            showLabel: false,
            tabStyle: {
              backgroundColor: "#A1887F",
              height: 65,
            },
          }}
        >
          <Tab.Screen name="Home" component={homescreen} />
          <Tab.Screen name="My To Do List" component={activity2} />
          <Tab.Screen name="Completed Tasks" component={profilescreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
};
export default App;