import React from 'react';
import { View, Text, StyleSheet } from "react-native";

const task = (props) => {

    return (
        <View style={styles.item}>
            <View style = {style.itemLeft}>
                <View style= {styles.square}></View>
                <Text style= {styles.itemText}>{props.text}</Text>
            </View>
            <View style= {styles.circular}></View>
        </View>
    );
};

    const styles = StyleSheet.create({
        item:{
            backgroundColor: "Pink",
            padding: 15,
            borderRadius: 10,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: 20,
            borderColor: "black",
        },
        itemLeft:{
            flexDirection: "row",
            alignItems: "center",
            flexWrap: "wrap",
        },
        square: {
            width: 24,
            height: 24,
            backgroundColor: "black",
            opacity: 0.4,
            borderRadius: 5,
            marginRight: 15,
        },
        itemText:{
            color: "black",
            maxWidth: "80%",
        },
        circular: {
            width: 12,
            height: 12,
            boderColor: "black",
            borderEndWidth: 2,
            borderRadius: 5,
        },
    });
    export default task;