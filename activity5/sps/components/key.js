import React, { Component } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'


class key extends Component {

    handleOperation = () => {
    this.props.operation(this.props.char)
    }

    componentDidMount = () => {
    }

    render() {
    return (
    <TouchableOpacity
        onPress={() => this.props.operation(this.props.char)}
        style={styles.container}
    >
        <Text style={styles.text}>{this.props.char}</Text>
    </TouchableOpacity>
    )
    }
}

const styles = StyleSheet.create({
    container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    },
    text: {
    color: '#ffb6c1',
    fontWeight: 'bold',
    fontSize: 30

    }
})

export default key;