import React from "react";
import { Image, Text, View } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

const slides = [
    {
    key: "one",
    title: "WHY I CHOSE IT?", 
    text: 
    "Let's Explore!",
    image: require("./image/picture1.png"),
    },
    {
    key: "two",
    title: "IT COURSE MEANS:",
    text:
    " It utilize of both hardware and software technologies to provide the computing solution that address the needs of many users.",
    image: require("./image/picture6.png"), 
    },
    {
    key: "three",
    title: "CHOOSING IT",
    text:
    "Choosing IT course is really challenging yet enjoying because i really learn a lot and understand what really IT is.",
    image: require("./image/picture4.png"),
    },
    {
    key: "four",
    title: "CHOOSING IT",
    text:
    "I chose IT because i love digital works like: video editing and photos and many more.",
    image: require("./image/image1.png"),
    },
    {
    key: "five",
    title: "OPPURTUNITIES",
    text:
    "Having an oppurtunities to learn IT is great, in future there is also a lot of job that offers.",
    image: require("./image/picture5.png"),
    },
    {
    key: "six",
    title: "REALIZTION",
    text:
    "I realize that IT course is that kind that we can say it is easy but even though it's really challenging although I still want to leran more about it.",
    image: require("./image/picture3.png"),
    },
]

const Slide = ({ item }) => {
    return (
    <View style={{ flex: 1, backgroundColor: 'purple'}}>
        <Image source={item.image}
        style={{
            resizeMode: 'contain',
            height: "70%",
            width: "100%",
        }}
        />
        <Text style={{
            paddingTop: 5,
            paddingBottom: 5,
            fontSize: 20,
            fontWeight: "bold",
            color: "#808080",
            alignSelf: "center",
        }}
        >
        {item.title}
        </Text>

        <Text style={{
        textAlign:"center",
        color:"#000000",
        fontSize:15,
        paddingHorizontal:20,
        }}>
        {item.text}
        </Text>
    </View>
    )
    };

    export default function IntroSliders({navigation}) {
    return(
        <AppIntroSlider
        renderItem={({item}) => <Slide item={item}/> } 
        data={slides} 
        activeDotStyle={{
            backgroundColor:"#5f9ea0",
            width:10
        }}
        onDone={() => navigation.push('Intro')}
        showDoneButton={true}
        renderDoneButton={()=> <Text style={{color: '#696969', fontWeight: 'bold', margin: 1, fontSize: 15}}>
            Home</Text>}
        showNextButton={true}
        renderNextButton={()=> <Text style={{color: '#696969', fontWeight: 'bold', margin: 1, fontSize: 15}}>
            Continue</Text>}
        /> 
    ) 
};